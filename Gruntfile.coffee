module.exports = (grunt) ->

  grunt.initConfig
    pkg:
      grunt.file.readJSON 'package.json'
    bowerDirectory:
      require('bower').config.directory

    clean:
      build:
        src: ['assets/css', 'assets/scripts', 'assets/fonts/']

    connect:
      serve:
        options:
          port: grunt.option('port') || '8000'
          hostname: grunt.option('host') || 'localhost'

    copy:
      #bootstrap-less:
      #  src: '<%= bowerDirectory %>/bootstrap/less/bootstrap.less'
      #  dest: 'tmp/'
      bootstrap:
        src: '<%= bowerDirectory %>/bootstrap/fonts/*'
        dest: 'assets/fonts/'

    cssmin:
      minify:
        expand: true
        cwd: 'assets/css'
        src:['*.css', '!*.min.css']
        dest: 'assets/css'
        ext: '.min.css'

    less:
      dist:
        options:
          compress: false
          paths: ['less', '<%= bowerDirectory %>/bootstrap/less']
        files:
          'assets/css/bootstrap.css': ['less/theme.less']

    recess:
      dist:
        options:
          compile: true
        files:
          'assets/css/bootstrap.css': ['assets/css/bootstrap.css']

    watch:
      less:
        files: ['less/*.less']
        tasks: ['recess']
        options:
          livereload: true
      cssmin:
        files: ['assets/css/bootstrap.css']
        tasks: ['cssmin:minify']

    # Load all Grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

    # Register Grunt tasks
    grunt.registerTask 'default', [
      'clean:build'
      'copy:bootstrap'
      'less:dist'
      'recess'
      'cssmin:minify'
    ]
    grunt.registerTask 'serve', [
      'connect:serve'
      'watch'
    ]
