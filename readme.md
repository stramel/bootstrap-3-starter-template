## Main Theme

### Required Software
- Node.js
- NPM (Now included with Windows install)
- Git

### Getting Started
Run the following commands

- npm install
- bower install

### Files
- package.json - Use to test npm what dependencies are used.
- bower.json - Use to tell Bower what dependencies are used.
- Gruntfile.coffee - Provides the all building, testing, and previewing functionality
  - CoffeeScript and Javascript are supported by Grunt
